<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Virksomhed',
  'Opportunities' => 'Salgsmulighed',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Contacts' => 'Kontakter',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'Prospects' => 'Mål:',
  'ProjectTask' => 'Projektopgave',
  'Tasks' => 'Opgave',
  'KBContents' => 'Videnbase',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue detaljposter',
);