<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Oppgave',
  'Opportunities' => 'Muligheter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'KBContents' => 'KB-dokumenter',
  'Notes' => 'Notat',
  'RevenueLineItems' => 'Omsetningsposter',
);