<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Uzņēmums',
  'Opportunities' => 'Iespēja',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Contacts' => 'Kontaktpersonas',
  'Products' => 'Piedāvājuma rinda',
  'Quotes' => 'Piedāvājums',
  'Bugs' => 'Kļūda',
  'Project' => 'Projekts',
  'Prospects' => 'Mērķis',
  'ProjectTask' => 'Projekta uzdevums',
  'Tasks' => 'Uzdevums',
  'KBContents' => 'Zināšanu bāze',
  'Notes' => 'Piezīme',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);