<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Firma',
  'Opportunities' => 'Verkaufschance',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Contacts' => 'Kontakte',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'Prospects' => 'Zielkontakt',
  'ProjectTask' => 'Projektaufgabe',
  'Tasks' => 'Aufgabe',
  'KBContents' => 'Wissensdatenbank',
  'Notes' => 'Hinweis',
  'RevenueLineItems' => 'Umsatzposten',
);