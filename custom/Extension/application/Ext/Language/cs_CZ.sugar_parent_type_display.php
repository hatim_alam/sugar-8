<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Společnost',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Úkol',
  'Opportunities' => 'Příležitost',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyby',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Project' => 'Projekty',
  'ProjectTask' => 'Projektové úkoly',
  'Prospects' => 'Kontakt',
  'KBContents' => 'Znalostní báze',
  'Notes' => 'Poznámka',
  'RevenueLineItems' => 'Řádky tržby',
);