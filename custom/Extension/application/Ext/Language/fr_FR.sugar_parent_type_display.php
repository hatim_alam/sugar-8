<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contact',
  'Tasks' => 'Tâche',
  'Opportunities' => 'Affaire',
  'Products' => 'Ligne de devis',
  'Quotes' => 'Devis',
  'Bugs' => 'Bugs',
  'Cases' => 'Ticket',
  'Leads' => 'Lead',
  'Project' => 'Projet',
  'ProjectTask' => 'Tâche Projet',
  'Prospects' => 'Cible',
  'KBContents' => 'Base de connaissances',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Lignes de revenu',
);