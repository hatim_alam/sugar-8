<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Llogaria',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Detyrë',
  'Opportunities' => 'Mundësi:',
  'Products' => 'Artikulli i rreshtit të cituar',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabimet',
  'Cases' => 'Rast',
  'Leads' => 'Udhëheqje',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat e projektit',
  'Prospects' => 'Synim',
  'KBContents' => 'Baza e njohurisë',
  'Notes' => 'Shënim',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);