<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Oportunidad',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Errores',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'KBContents' => 'Base de Conocimiento',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);