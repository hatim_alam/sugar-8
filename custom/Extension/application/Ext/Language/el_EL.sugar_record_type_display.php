<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Λογαριασμός',
  'Opportunities' => 'Ευκαιρία',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Contacts' => 'Επαφές',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλμα',
  'Project' => 'Έργο',
  'Prospects' => 'Στόχος',
  'ProjectTask' => 'Εργασία Έργου',
  'Tasks' => 'Εργασία',
  'KBContents' => 'Βάση Γνώσεων',
  'Notes' => 'Σημείωση',
  'RevenueLineItems' => 'Στοιχεία γραμμής εσόδων',
);