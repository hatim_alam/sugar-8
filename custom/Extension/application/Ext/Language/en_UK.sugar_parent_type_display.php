<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Note',
  'RevenueLineItems' => 'Revenue Line Items',
);