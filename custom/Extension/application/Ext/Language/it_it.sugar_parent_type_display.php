<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Tasks' => 'Compito',
  'Opportunities' => 'Opportunità',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);