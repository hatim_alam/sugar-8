<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Сделка',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибки',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'KBContents' => 'База знаний',
  'Notes' => 'Примечание',
  'RevenueLineItems' => 'Доходные продукты',
);