<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Azienda',
  'Opportunities' => 'Opportunità',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Contacts' => 'Contatti',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Project' => 'Progetto',
  'Prospects' => 'Obiettivo',
  'ProjectTask' => 'Compiti di Progetto',
  'Tasks' => 'Compito',
  'KBContents' => 'Knowledge Base',
  'Notes' => 'Nota',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);