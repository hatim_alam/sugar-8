<?php
 // created: 2018-05-01 17:58:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Uppgift',
  'Opportunities' => 'Affärsmöjlighet',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Buggar',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektuppgift',
  'Prospects' => 'Mål',
  'KBContents' => 'Kunskapsbas',
  'Notes' => 'Anteckning',
  'RevenueLineItems' => 'Intäktsposter',
);